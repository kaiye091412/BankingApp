package BankingAppPt1.part1;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import Account.Account;
import Users.Customer;
import Users.User.Role;

/**
 * Unit test for simple App.
 */
public class AppTest{
	Customer c = new Customer();
	ArrayList<Customer> listCustomer = new ArrayList<Customer>();
	Account account = new Account(0,listCustomer);
	@Test
	public void test1() {  
		System.out.println("Account Id test");
		Assertions.assertEquals(0, account.getId());
	}
	@Test
	public void test2() {
		System.out.println("Customer role test");
		Assertions.assertEquals(Role.CUSTOMER, c.getRole());
	}
}
