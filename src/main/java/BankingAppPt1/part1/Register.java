package BankingAppPt1.part1;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.Collections;
import java.util.HashMap;
import java.util.Scanner;

import Users.Customer;
import Users.User.Role;

public class Register {
	private Customer customer; //new customer to be added
	private Scanner sc = App.sc;
	private HashMap<String, Customer> cd = App.cd;
	
	public Register() {
		
	}
	public void start() { //constructor starts chain of calling for user inputs
		this.customer = new Customer();
		username();
	}
	private void username() {
		System.out.println("Username:");
		String input = sc.next();
		while(cd.get(input)!=null) { 
			System.out.println("Username exists. Try another one.");
			System.out.println("Username:");
			input = sc.next();
		}
		// at this point, you are sure this username is available
		customer.setUsername(input);
		password();
	}
	private void password() {
		System.out.println("Password:");
		String input1 = sc.next();
		while(input1.length()<8) { 
			System.out.println("Password length should be at least 8");
			System.out.println("Password:");
			input1 = sc.next();
		}
		
		System.out.println("Confirm Password:");
		String input2 = sc.next();
		if(!input2.equals(input1)) { // if doesn't match do password all over again
			System.out.println("Password doesn't match."); 
			password(); 
			return;
		}
		// at this point password confirmation is done
		customer.setPassword(input1);
		firstName();
	}
	private void firstName() {
		System.out.println("First Name:");
		String input = sc.next();
		customer.setFirstName(input);
		lastName();
	}
	private void lastName() {
		System.out.println("Last Name:");
		String input = sc.next();
		customer.setLastName(input);
		email();
	}
	private void email() {
		System.out.println("Email Address:");
		String input = sc.next();
		customer.setEmail(input);
		confirmRegister();
	}
	private void confirmRegister() {
		System.out.println("Y to Confirm Register");  //UI
		System.out.println("N to Discard");	//UI
		String input = sc.next();
		while(!input.equals("Y")&&!input.equals("N")) {
			System.out.println("Y to Confirm Register");  //UI
			System.out.println("N to Discard");	//UI
			input = sc.next();
		}
		if(input.equals("N")) {
			App.start();
			return;
		}
		if(input.equals("Y")) {
			cd.put(customer.getUsername(), customer);
		}
		App.start();
	}
	
}
