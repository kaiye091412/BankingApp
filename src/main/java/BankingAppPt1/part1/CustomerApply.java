package BankingAppPt1.part1;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

import Account.Account;
import Users.Customer;

public class CustomerApply {
	private Scanner sc = App.sc;
	private HashMap<String, Customer> cd = App.cd;
	private Customer applyee;
	private ArrayList<Account> accApplication =App.applications;  //to be seriliazed
	public CustomerApply(Customer applyee) {
		this.applyee = applyee;
		
	}

	public void start() {
		joint();
	}
	private void joint() { //takes care of account owners. no joint also here
		String input;
		ArrayList<Customer> owners = new ArrayList<Customer>();
		owners.add(applyee);
		do {
			System.out.println("Enter any joint users' username");
			System.out.println("No for no more joint users");
			input = sc.next();
			if(!input.equals("No")) {
				if(cd.get(input)!=null) owners.add(cd.get(input));
			}
			if(cd.get(input)==null && !input.equals("No")) System.out.println("Customer does not exist");
		}while(!input.equals("No"));
		
		Account toBeAdded = new Account(0,owners);
		confirm(toBeAdded);
	}
	
	private void confirm(Account toBeAdded) {
		String input;
		do {
			System.out.println("Confirm application? Y/N");
			input = sc.next();
		}while(!input.equals("Y") && !input.equals("N"));
		if(input.equals("Y")) {
			accApplication.add(toBeAdded);
			System.out.println("Account application submitted. Waiting for approval.");
			App.start();
		}
		if(input.equals("N")) {
			applyee.start();
			return;
		}
	}
}
