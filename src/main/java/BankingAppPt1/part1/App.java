package BankingAppPt1.part1;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;
import org.junit.Assert;
import org.junit.jupiter.api.Test;

import Account.Account;
import BankDAO.BankDAOImp;
import Users.Customer;
import Users.Employee;
import Users.User;
import Users.User.Role;

public class App
{
	public static Scanner sc = new Scanner(System.in);
	public static HashMap<String, Customer> cd;
	public static ArrayList<Account> applications;
	public static HashMap<Integer, ArrayList<Customer>> inuse;  //account id to customer
	
	
	private static void preload() {
		File f1 = new File("./users/customer.ser");
		File f2 = new File("./accounts/applications.ser");
		File f3 = new File("./accounts/inuse.ser");
		if(!f1.exists()) {  //first time only
	        try {
				ObjectOutputStream oos = new ObjectOutputStream( 
				        new FileOutputStream(f1));
		        oos.writeObject(new HashMap<String, Customer>());
		        oos.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}//first time only. create Customer.ser with empty hashmap
		try {
			FileInputStream fileIn = new FileInputStream("./users/customer.ser");
			ObjectInputStream in = new ObjectInputStream(fileIn);
			cd= (HashMap<String, Customer>) in.readObject();
			in.close();
		} catch (Exception e) {}
		/////
		if(!f2.exists()) {  //first time only
	        try {
				ObjectOutputStream oos = new ObjectOutputStream( 
				        new FileOutputStream(f2));
		        oos.writeObject(new ArrayList<Account>());
		        oos.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}//first time only. create Customer.ser with empty hashmap
		try {
			FileInputStream fileIn = new FileInputStream("./accounts/applications.ser");
			ObjectInputStream in = new ObjectInputStream(fileIn);
			applications= (ArrayList<Account>) in.readObject();
			in.close();
		} catch (Exception e) {}
		///////
		if(!f3.exists()) {  //first time only
	        try {
				ObjectOutputStream oos = new ObjectOutputStream( 
				        new FileOutputStream(f3));
		        oos.writeObject(new HashMap<Integer, ArrayList<Customer>>());
		        oos.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}//first time only. create Customer.ser with empty hashmap
		try {
			FileInputStream fileIn = new FileInputStream("./accounts/inuse.ser");
			ObjectInputStream in = new ObjectInputStream(fileIn);
			inuse= (HashMap<Integer, ArrayList<Customer>>) in.readObject();
			in.close();
		} catch (Exception e) {}
	} // end of preload
	
	
	public static void start() {
		
    	System.out.println("Welcome to KH Bank.");
    	System.out.println("Login or Register");
    	System.out.println("Quit");
    	String input = sc.next();
        while((!input.equals("Login")) && (!input.equals("Register") &&(!input.equals("Quit")))) {
        	System.out.println("Login or Register");
        	input = sc.next();
        }
        if(input.equals("Register")) {
        	Register register = new Register();
        	register.start();
        }
        if(input.equals("Login")) {
        	Login login = new Login();
        	login.start();
        }
        if(input.equals("Quit")) {
        	return;
        }
	}
		
		
	
    public static void main( String[] args )
    {
    	BankDAOImp dao = new BankDAOImp(args[0]);
    	dao.addUser("kai4", "123", "wef","wef","wef");
    	
    	/*
    	preload();
    	start();
    	//save to files before quiting banking app
        try {
			ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream("./users/customer.ser"));
	        oos.writeObject(cd);
	        oos.close();
	        oos = new ObjectOutputStream(new FileOutputStream("./accounts/applications.ser"));
	        oos.writeObject(applications);
	        oos.close();
			oos = new ObjectOutputStream(new FileOutputStream("./accounts/inuse.ser"));
	        oos.writeObject(inuse);
	        oos.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
    	*/
    }
}
