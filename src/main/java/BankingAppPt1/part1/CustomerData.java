package BankingAppPt1.part1;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.HashMap;

import Users.Customer;
import Users.User;

public class CustomerData {
	private HashMap<String, Customer> customerData;
	private String fileName= "./users/customer.ser";
	
	public CustomerData(){
		File f = new File("./users/customer.ser");
		if(!f.exists()) {  //first time only
	        try {
				ObjectOutputStream oos = new ObjectOutputStream( 
				        new FileOutputStream(f));
		        oos.writeObject(new HashMap<String, Customer>());
		        oos.close();
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}//first time only. create Customer.ser with empty hashmap
		
		try {
			FileInputStream fileIn = new FileInputStream(fileName);
			ObjectInputStream in = new ObjectInputStream(fileIn);
			customerData= (HashMap<String, Customer>) in.readObject();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}
	
	public Customer get(String s){
		return customerData.get(s);
	}
	public void put(String s, Customer c) {
		customerData.put(s, c);
        try {
			ObjectOutputStream oos = new ObjectOutputStream( 
			        new FileOutputStream(fileName));
	        oos.writeObject(customerData);
	        oos.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	public HashMap<String, Customer> getMap (){
		return customerData;
	}
}
