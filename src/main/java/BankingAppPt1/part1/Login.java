package BankingAppPt1.part1;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.HashMap;
import java.util.Scanner;

import Users.Admin;
import Users.Customer;
import Users.Employee;
import Users.User;

public class Login {
	private Scanner sc = App.sc;
	private HashMap<String, Customer> cd = App.cd;
	private String username;
	
	public Login() {
	}
	
	public void start() {
		username = new String();
		username();
	}
	
	private void username() {
		System.out.println("Username:");
		String input = sc.next();
		while(input==null ||input.equals("")) {  //make sure username input is legit
			System.out.println("Username:");
			input = sc.next();
		}
		if(input.equals("employee") || input.equals("admin")) { ////////  employee login hardcoded for demo.
			username = input; ////////
			password();
			return;
		}//////
		if(cd.get(input)==null) {
			System.out.println("Username does not exist.");
			do {
				System.out.println("Register or Redo");
				input = sc.next();
				if(input.equals("Register")) {
					Register register = new Register();
					register.start();  //this instance of login still exits, might be a problem
					return;
				}
				if(input.equals("Redo")) {
					username();
					return;
				}
			}while(!input.equals("Register") &&!input.equals("Redo"));
		}
		//at this point, username is put in correctly 
		username = input;
		password();
	}
	
	private void password() {
		System.out.println("Password:");
		String input = sc.next();
		if(username.equals("employee")) { //////  employee login hardcoded for demo.
			if(!input.equals("12345678")) {
				System.out.println("Wrong password. Do it again.");
				this.start();
				return;
			}
			else {
				System.out.println("You have logged in as employee");
				Employee emp = new Employee();
				emp.start();
				return;
			}
		}///////////////
		if(username.equals("admin")) { //////  employee login hardcoded for demo.
			if(!input.equals("12345678")) {
				System.out.println("Wrong password. Do it again.");
				this.start();
				return;
			}
			else {
				System.out.println("You have logged in as admin");
				Admin admin = new Admin();
				admin.start();
				return;
			}
		}///////////////
		if(!input.equals(cd.get(username).getPassword())) {
			System.out.println("Wrong password. Do it again.");
			this.start();
			return;
		}
		if(input.equals(cd.get(username).getPassword())) {
			System.out.println("You have logged in as "+username);
			cd.get(username).start();
		}
	}

}
