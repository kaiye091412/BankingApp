package Users;

import java.io.Serializable;

public abstract class User implements Serializable{
	
	protected String username;
	protected String password;
	protected Role role;
	
	public enum Role{
		CUSTOMER, EMPLOYEE, ADMIN 
	}
	
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
	public Role getRole() {
		return role;
	}
	public void setRole(Role role) {
		this.role = role;
	}
}
