package Users;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

import Account.Account;
import BankingAppPt1.part1.App;
import BankingAppPt1.part1.CustomerApply;

public class Customer extends User{
	private String firstName;
	private String lastName;
	private String email;
	
	//private Date dob;
	private ArrayList<Account> accountOwned;  //can own more than one accounts
	private transient Scanner sc = App.sc;
	private transient HashMap<Integer, ArrayList<Customer>> inuse = App.inuse;
	
	public Customer(String username, String password, String lastName, String firstName, String email) {
		this.username = username;
		this.password = password;
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.role = Role.CUSTOMER;
		accountOwned = new ArrayList<Account>();
	}
	
	public Customer() { 
		
	}
	
	public ArrayList<Account> getAccountOwned(){
		return accountOwned;
	}
	

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
	public void start() {
		sc = App.sc;
		String input;
		do {
			System.out.println("Apply to apply for a new account");
			System.out.println("View to view accounts");
			System.out.println("Logout");
			input = sc.next();
		}while(!input.equals("Apply")&&!input.equals("View")&&!input.equals("Logout"));
		if(input.equals("Apply")) {
			apply();
		}
		if(input.equals("View")) {
			view();
		}
		if(input.equals("Logout")) {
			App.start();
		}
	}
	private void apply() {
		CustomerApply customerApply = new CustomerApply(this);
		customerApply.start();
	}
	private void view() {
		if(accountOwned ==null ||accountOwned.size()==0) {
			System.out.println("You do not have an account.");
			this.start();
			return;
		}
		for(Account acc:accountOwned) {
			System.out.println("ID: "+acc.getId());
			System.out.println("Balance: "+acc.getBalance());
		}
		System.out.println(" ");
		String input;
		do {
			System.out.println("Deposit");
			System.out.println("Withdraw");
			System.out.println("Transfer");
			System.out.println("Back");
			input = sc.next();
		}while(!input.equals("Deposit") && !input.equals("Withdraw")
				&&!input.equals("Transfer")&&!input.equals("Back"));
		if(input.equals("Deposit")) {
			deposit();
		}
		if(input.equals("Withdraw")) {
			withdraw();
		}
		if(input.equals("Transfer")) {
			transfer();
		}
		if(input.equals("Back")) {
			this.start();
		}
	}
	
	private void deposit() {
		String input;
		int ID =0;
		int inputInt =0;
		boolean temp =true;//true if Acc Id and input do NOT match, another input needed
		do {
			System.out.println("Account ID: ");
			input = sc.next();
			for(Account a: accountOwned) {
				if(Integer.toString(a.getId()).equals(input)) { //match
					temp = false;
					break;
				}
			}
		}while(temp);
		ID = Integer.parseInt(input);
		
		temp = false;
		do {
			System.out.println("Amount: (1-3000)");
			input = sc.next();
			try {
				inputInt = Integer.parseInt(input);
			}catch(NumberFormatException e) {
				System.out.println("Number format wrong");
				temp = true;
			}
		}while(temp);
		
		if(inputInt<1 || inputInt>3000) {
			System.out.println("Your deposit amount is wrong!");
			this.start();
		}
		for(Account a :this.accountOwned) {
			if(a.getId()==ID) {
				a.deposit(inputInt);
				break;
			}
		}
		this.start();
	}
	
	private void withdraw() {
		String input;
		int ID =0;
		int inputInt =0;
		boolean temp =true;//true if Acc Id and input do NOT match, another input needed
		do {
			System.out.println("Account ID: ");
			input = sc.next();
			for(Account a: accountOwned) {
				if(Integer.toString(a.getId()).equals(input)) { //match
					temp = false;
					break;
				}
			}
		}while(temp);
		ID = Integer.parseInt(input);
		
		temp = false;
		do {
			System.out.println("Amount: (1-3000)");
			input = sc.next();
			try {
				inputInt = Integer.parseInt(input);
			}catch(NumberFormatException e) {
				System.out.println("Number format wrong");
				temp = true;
			}
		}while(temp);
		
		if(inputInt<1 || inputInt>3000) {
			System.out.println("Your amount is wrong!");
			this.start();
		}
		for(Account a :this.accountOwned) {
			if(a.getId()==ID) {
				a.withdraw(inputInt);
				break;
			}
		}
		this.start();
	}
	
	private void transfer(){
		String input;
		int fromID =0;
		int inputInt =0;
		boolean temp =true;//true if Acc Id and input do NOT match, another input needed
		do {
			System.out.println("From Account ID: ");
			input = sc.next();
			for(Account a: accountOwned) {
				if(Integer.toString(a.getId()).equals(input)) { //match
					temp = false;
					break;
				}
			}
		}while(temp);
		fromID = Integer.parseInt(input);
		
		temp = false;
		do {
			System.out.println("Amount: (1-3000)");
			input = sc.next();
			try {
				inputInt = Integer.parseInt(input);
			}catch(NumberFormatException e) {
				System.out.println("Number format wrong");
				temp = true;
			}
		}while(temp);
		
		if(inputInt<1 || inputInt>3000) {
			System.out.println("Your amount is wrong!");
			this.start();
		}
		

		do {
			System.out.println("To Account ID: ");
			input = sc.next();
		}while(!inuse.containsKey(Integer.parseInt(input)));
		int toID = Integer.parseInt(input);
		for(Account b : this.accountOwned) {	//withdraw from
			if(b.getId()==toID) {
				b.withdraw(inputInt);
			}
		}//
		ArrayList<Customer> owners = inuse.get(toID);  //deposit to
		for(Customer c:owners) {
			for(Account a :c.getAccountOwned()) {
				a.deposit(inputInt);
			}
		}
		this.start();
	}
	
	public void print() {
		System.out.println("Name: " + this.firstName +" "+ this.lastName);
		System.out.println("Email: " + this.email);
		if(accountOwned==null||accountOwned.size()==0) {
			System.out.println("No account owned");
			return;
		}
		for(Account a:this.accountOwned) {
			System.out.println("Account ID: "+a.getId());
			System.out.println("Balance: "+ a.getBalance());
		}
	}
	

	/*
	 *  public Date getDob() {
		return dob;
	}

	public void setDob(Date dob) {
		this.dob = dob;
	}
	*/
	
}
