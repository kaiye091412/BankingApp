package Users;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.Scanner;

import Account.Account;
import BankingAppPt1.part1.App;
import BankingAppPt1.part1.CustomerData;

public class Employee extends User{
	private transient Scanner sc = App.sc;
	private transient ArrayList<Account> applications = App.applications;
	private transient HashMap<Integer, ArrayList<Customer>> inuse = App.inuse;
	private transient HashMap<String, Customer> cd = App.cd;
	
	public void start() {
		String input;
		do {
			System.out.println("1: View account applications");
			System.out.println("2: View customers");
			System.out.println("Logout");
			input = sc.next();
		}while(!input.equals("1") && !input.equals("2") && !input.equals("Logout"));
		if(input.equals("1")) {
			viewApplications();
		}
		if(input.equals("2")) {
			viewCustomers();
		}
		if(input.equals("Logout")) {
			App.start();
			return;
		}
	}
	
	void viewApplications() {
		
		for(int i=1;i<applications.size()+1;i++) {
			System.out.println(i+": ");
			applications.get(i-1).print();
		}
		String input;
		do {
			System.out.println("Approve, Deny or Back");
			input = sc.next();
		}while(!input.equals("Approve")&& !input.equals("Deny")&&!input.equals("Back"));
		if(input.equals("Approve")) {
			int inputInt;
			do {
				System.out.println("which one");
				input = sc.next();
				try {
					inputInt = Integer.parseInt(input);
				}catch(NumberFormatException e) { //error check. when inputInt<1 it will loop back
					inputInt = -1;
				}
			}while(inputInt<1 ||inputInt>applications.size());
			//approve logics start here
			Account temp = applications.get(inputInt-1);
			temp.setId(inuse.size()+100); //ID starts from 100 and incremented by 1
			inuse.put(temp.getId(),temp.getOwner());
			
			ArrayList<Customer> owners = temp.getOwner();
			for(Customer c :owners) {
				c.getAccountOwned().add(temp);
				cd.put(c.getUsername(), c);
			}
			
			applications.remove(inputInt-1);
			this.start();
			return;
		}
		if(input.equals("Deny")) {
			int inputInt;
			do {
				System.out.println("which one");
				input = sc.next();
				try {
					inputInt = Integer.parseInt(input);
				}catch(NumberFormatException e) { //error check. when inputInt<1 it will loop back
					inputInt = -1;
				}
			}while(inputInt<1 ||inputInt>applications.size());
			//approve logics start here

			applications.remove(inputInt-1);
			this.start();
			return;
		}
		if(input.equals("Back")) {
			this.start();
			return;
		}
		
	}
	void viewCustomers() {
		Iterator<Entry<String, Customer>> it = cd.entrySet().iterator();
		int i =1;
		while(it.hasNext()) {
			System.out.println(i++);
			Entry<String, Customer> entry = it.next();
			entry.getValue().print();
		}
		System.out.println("");
		this.start();
		return;
	}
}
