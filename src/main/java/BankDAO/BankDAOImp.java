package BankDAO;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


import Account.Account;
import Users.Customer;
import Users.User;

public class BankDAOImp implements BankDAO{
    private static String url = "jdbc:oracle:thin:@revaturedatabase.cgfxycwbhqwa.us-east-2.rds.amazonaws.com:1521:ORCL";
    private static String username = "kaiye091412";
    private static String password;
    private Connection conn;
    
    public BankDAOImp(String dbPassword) {   //take from stdin and pass here
    	password = dbPassword;
    	try{  								//connect and have the connection open
    		conn = DriverManager.getConnection(url, username, password);
        }catch(SQLException e) {
            e.printStackTrace();
        }
    }
    
	@Override
	public int addUser(String username, String password, String lastname, String firstname, String email) {
        String sql = 
        		" BEGIN "+
        		"insert_User(?, ?, ?, ?, ?);" +
        		"END;"
        		;
        try {
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, username);
			ps.setString(2, password);
			ps.setString(3, lastname);
			ps.setString(4, firstname);
			ps.setString(5, email);
			return ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
        
		return -2; // should not be here. -2 something strong and wrong
	}
	@Override
	public int deleteUser() {
		// TODO Auto-generated method stub
		return 0;
	}
	
	@Override
	public User selectUser(String loginName) {
        String sql = "select * from Users ";
        User user = null;
        try {
			PreparedStatement ps = conn.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            
            while(rs.next()) {
            	user = new Customer(rs.getString(1), rs.getString(2),rs.getString(3), rs.getString(4), rs.getString(5));
            }
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return user;
	}
	
	@Override
	public int addAccount() {
		// TODO Auto-generated method stub
		return 0;
	}
	@Override
	public int deleteAccount() {
		// TODO Auto-generated method stub
		return 0;
	}
	@Override
	public Account selectAccountById() {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public List<Account> selectAccountsOwned(Customer c) {
		// TODO Auto-generated method stub
		return null;
	}
    
    
}
