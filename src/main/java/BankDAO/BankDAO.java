package BankDAO;

import java.util.List;

import Account.*;
import Users.Customer;
import Users.User;

public interface BankDAO {
	public int addUser(String username, String password, String lastname, String firstname, String email);
	public int deleteUser();
	public User selectUser(String loginName);
	public int addAccount();
	public int deleteAccount();
	public Account selectAccountById();
	public List<Account> selectAccountsOwned(Customer c);
	
}
