package Account;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;

import Users.Customer;

public class Account implements Serializable{
	private int id=0;
	private ArrayList<Customer> owner;  //can be more than one owners
	private float balance;
	
	public Account(int id, ArrayList<Customer> owner) {
		this.id = id;  // 0 for account in application process //real Id starts from 100
		this.owner = owner;
		this.balance = 0;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public ArrayList getOwner() {
		return owner;
	}

	public void setOwner(ArrayList owner) {
		this.owner = owner;
	}

	public float getBalance() {
		return balance;
	}

	public void setBalance(float balance) {
		this.balance = balance;
	}
	
	public void deposit(int in) {
		if(in<1 ||in>3000) {
			System.out.println("Deposit amount wrong");
			return;
		}
		this.balance +=in;
		System.out.println("You have deposited "+in+" to Account ID: "+this.id);
	}
	public void withdraw(int in) {
		if(in<1 ||in>3000) {
			System.out.println("withdraw amount wrong");
			return;
		}
		this.balance -=in;
		System.out.println("You have withdrew "+in+" from Account ID: "+this.id);
		
	}
	
	public void print() {
		System.out.print("Owners:");
		for(int i =0; i<this.owner.size(); i++) {
			System.out.print(" "+owner.get(i).getUsername());
		}
		System.out.println();
		System.out.println("Balance: "+ balance);
	}

	
}
